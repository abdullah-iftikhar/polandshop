@extends('front.layouts.main')
@section('title', 'Login/Register')

@section('front-content')

    <!-- Breadcrumb Start -->
    <div class="breadcrumb-wrap">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Login & Register</li>
            </ul>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Login Start -->
    <div class="login">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <form action="" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="register-form">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Name</label>
                                    <input class="form-control" type="text" name="name" value="{{old('name')}}"
                                           placeholder="Write Name">
                                    @if($errors->has('name'))
                                        <div class="error"
                                             style="color:red">{{$errors->first('name')}}</div>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label>Username</label>
                                    <input class="form-control" type="text" name="username" value="{{old('username')}}"
                                           placeholder="Write Username">
                                    @if($errors->has('username'))
                                        <div class="error"
                                             style="color:red">{{$errors->first('username')}}</div>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label>E-mail</label>
                                    <input class="form-control" type="email" name="email" value="{{old('email')}}"
                                           placeholder="Write E-mail">
                                    @if($errors->has('email'))
                                        <div class="error"
                                             style="color:red">{{$errors->first('email')}}</div>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label>Mobile No</label>
                                    <input class="form-control" type="text" name="mobil_no" value="{{old('mobil_no')}}"
                                           placeholder="Write Mobile No">
                                    @if($errors->has('mobil_no'))
                                        <div class="error"
                                             style="color:red">{{$errors->first('mobil_no')}}</div>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label>Password</label>
                                    <input class="form-control" type="password" name="password"
                                           value="{{old('password')}}" placeholder="Write Password">
                                    @if($errors->has('password'))
                                        <div class="error"
                                             style="color:red">{{$errors->first('password')}}</div>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label>Retype Password</label>
                                    <input class="form-control" type="password" name="re_password"
                                           value="{{old('re_password')}}" placeholder="Re-Password">
                                    @if($errors->has('re_password'))
                                        <div class="error"
                                             style="color:red">{{$errors->first('re_password')}}</div>
                                    @endif
                                </div>
                                <div class="col-md-12">
                                    <button class="btn" type="submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-6">
                    <form action="" method="post">
                        @csrf
                        <div class="login-form">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>E-mail</label>
                                    <input class="form-control" type="email" name="email" placeholder="Write E-mail">
                                    @if($errors->has('email'))
                                        <div class="error"
                                             style="color:red">{{$errors->first('email')}}</div>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label>Password</label>
                                    <input class="form-control" type="password" name="password" placeholder="Password">
                                    @if($errors->has('password'))
                                        <div class="error"
                                             style="color:red">{{$errors->first('password')}}</div>
                                    @endif
                                </div>
                                <div class="col-md-12">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="newaccount">
                                        <label class="custom-control-label" for="newaccount">Keep me signed in</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Login End -->

@endsection
