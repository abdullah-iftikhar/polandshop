<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => 'auth', 'after' => 'no-cache'], function () {
    Route::group(['middleware' => 'admin'], function () {
        Route::prefix('admin')->group(function () {
            //Dashboard and Profile
            Route::get('/dashboard', 'App\Http\Controllers\Admin\DashboardController@dashboard')->name('admin.dashboard');
            Route::get('settings/profile', 'App\Http\Controllers\Admin\DashboardController@profileIndex')->name('admin.settings.profile');
            Route::post('profile/update', 'App\Http\Controllers\Admin\DashboardController@updateProfile')->name('admin.update.profile');
            //End Dashboard and Profile

//            Start Category
            Route::resource('category', 'App\Http\Controllers\Admin\CategoryController', [
                'names' => [
                    'index' => 'category.index',
                    'create' => 'category.create',
                    'store' => 'category.store',
                    'edit' => 'category.edit',
                    'update' => 'category.update',
                ],
                'except' => ['show', 'destroy']
            ]);
            Route::post('category/page/result', 'App\Http\Controllers\Admin\CategoryController@getPageResult')->name('category.get_page_result');
            Route::put('category/status/{id}', 'App\Http\Controllers\Admin\CategoryController@categoryStatus')->name('category.status');
            Route::delete('category/delete/{id}', 'App\Http\Controllers\Admin\CategoryController@destroy')->name('category.drop');
//            End Category

            //Start Item
            Route::resource('item', 'App\Http\Controllers\Admin\ItemController', [
                'names' => [
                    'index' => 'item.index',
                    'create' => 'item.create',
                    'store' => 'item.store',
                    'edit' => 'item.edit',
                    'update' => 'item.update',
                ],
                'except' => ['show', 'destroy']
            ]);
            Route::post('item/page/result', 'App\Http\Controllers\Admin\ItemController@getPageResult')->name('item.get_page_result');
            Route::put('item/status/{id}', 'App\Http\Controllers\Admin\ItemController@itemStatus')->name('item.status');
            Route::delete('item/delete/{id}', 'App\Http\Controllers\Admin\ItemController@destroy')->name('item.drop');
            //End Item

        });
    });
});
