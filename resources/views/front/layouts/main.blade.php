<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Poland Stop N Shop</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Polanstopnshop" name="keywords">
    <meta content="Polanstopnshop" name="description">

    <title>@yield('title', 'Poland Stop N Shop')</title>


    <!-- Favicon -->
    <link href="{{asset('front-assets/img/favicon.png')}}" rel="icon">

    <!-- CSS Libraries -->
{{--    <link href="{{asset('front-assets/css/fonts.css')}}" rel="stylesheet">--}}
    <link href="{{asset('front-assets/fonts/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <link href="{{asset('front-assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('front-assets/css/all.min.css')}}" rel="stylesheet">
    <link href="{{asset('front-assets/lib/slick/slick.css')}}" rel="stylesheet">
    <link href="{{asset('front-assets/lib/slick/slick-theme.css')}}" rel="stylesheet">
    <!-- Template Stylesheet -->
    <link href="{{asset('front-assets/css/style.css')}}" rel="stylesheet">
    @stack('front.extra-css')
</head>
<body>
@if ($message = Session::get('success'))
    <div class="alert alert-success mb-2"
         style="width: 500px;position: absolute;right: 0;top: 40px;z-index:1;background: #53ab5a;color: white;padding: 10px"
         id="alert-success-message" role="alert">
        <strong>Success! </strong> {{$message}}
    </div>
@endif
@if ($message = Session::get('error'))
    <div class="alert alert-danger mb-2" id="alert-error-message"
         style="width: 500px;position: absolute;right: 0;top: 40px;z-index:1;background: red;color: white;padding: 10px;"
         role="alert">
        <strong>Error! </strong> {{$message}}
    </div>
@endif

@include('front.layouts.header')

@yield('front-content')

@include('front.layouts.footer')


<!-- JavaScript Libraries -->
<script src="{{asset('front-assets/js/jquery.min.js')}}"></script>
<script src="{{asset('front-assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('front-assets/lib/easing/easing.min.js')}}"></script>
<script src="{{asset('front-assets/lib/slick/slick.min.js')}}"></script>
<!-- Template Javascript -->
<script src="{{asset('front-assets/js/main.js')}}"></script>

@if ($message = Session::get('success'))
    <script>
        setTimeout(function () {
            document.getElementById('alert-success-message').style.display = 'none'
        }, 3000);
    </script>
@endif
@if ($message = Session::get('error'))
    <script>
        setTimeout(function () {
            document.getElementById('alert-error-message').style.display = 'none'
        }, 3000);
    </script>
@endif
 @stack('front.extra-js')
</body>
</html>

