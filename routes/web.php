<?php

use App\Models\Item;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\Frontend\HomeController@homePage')->name('front.home');


Route::get('/cart', function () {
    return view('front.cart');
})->name('front.cart');

Route::get('/checkout', function () {
    return view('front.checkout');
})->name('front.checkout');

//Route::get('/categories', function () {
//    return view('front.category');
//})->name('front.categories');

Route::get('/best-seller', function () {
    return view('front.best-seller');
})->name('front.best.seller');

Route::get('/my-account', function () {
    return view('front.my-account');
})->name('front.account');

//Category Products
Route::get('/category/{slug}', 'App\Http\Controllers\Frontend\ProductController@getCategoryPro')->name('front.category.products');

//Products
Route::get('/product-detail/{slug}', 'App\Http\Controllers\Frontend\ProductController@productDetail')->name('front.product.detail');
Route::get('/product-list', 'App\Http\Controllers\Frontend\ProductController@productList')->name('front.product.list');

Route::get('/wishlist', function () {
    return view('front.wishlist');
})->name('front.wishlist');

Route::get('/login', function () {
    return view('front.login');
})->name('front.login');

// Route::get('/forgot', function () {
//     return view('front.forgot');
// })->name('front.forgot');

Route::get('/contact', function () {
    return view('front.contact');
})->name('front.contact');


 Route::get('/logout', function () {
     \Illuminate\Support\Facades\Auth::logout();
     return view('admin.admin_login');
 })->name('logout');


//User
Route::post('register/to/user' , 'App\Http\Controllers\AuthController@userRegister')->name('user.register');
Route::post('login/to/user' , 'App\Http\Controllers\AuthController@userLogin')->name('user.login');
Route::post('forgot/password' , 'App\Http\Controllers\AuthController@forgotPassword')->name('forgot.password');
//Admin
Route::get('/admin/login', function () {
    return view('admin.admin_login');
})->name('administrator.login');

Route::post('login/to/admin' , 'App\Http\Controllers\AuthController@adminLogin')->name('admin.login');

//Clear Cache and config
Route::get('/clear-cache', function () {
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    return "Cache is cleared";
});
Route::get('/config-cache', function () {
    \Illuminate\Support\Facades\Artisan::call('config:cache');
    return "Cache is cleared";
});

include "admin.php";
include "user.php";

