<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => 'auth', 'after' => 'no-cache'], function () {
    Route::group(['middleware' => 'user'], function () {
        Route::prefix('user')->group(function () {
//            Dashboard and Profile
//            Route::get('/dashboard', 'App\Http\Controllers\User\DashboardController@dashboard')->name('user.dashboard');
//            Route::get('edit/profile', 'App\Http\Controllers\User\DashboardController@profileIndex')->name('user.edit.profile');
//            Route::post('profile/update', 'App\Http\Controllers\User\DashboardController@updateProfile')->name('user.update.profile');

            //Items
//            Route::get('add/item', 'App\Http\Controllers\User\DashboardController@userItemCreate')->name('user.add.item');
//            Route::post('post/item/data', 'App\Http\Controllers\User\DashboardController@itemPostData')->name('user.post.item.data');
//            Route::get('edit/item/{id}', 'App\Http\Controllers\User\DashboardController@itemEdit')->name('user.edit.item');
//            Route::post('update/item/data/{id}', 'App\Http\Controllers\User\DashboardController@updateItemData')->name('user.update.item.data');
//            Route::get('delete/item/{id}', 'App\Http\Controllers\User\DashboardController@itemDelete')->name('user.delete.item');

            //Comment
//            Route::post('post/comment/{id}', 'App\Http\Controllers\User\DashboardController@postComment')->name('user.post.comment');
            //search
//            Route::post('search/items', 'App\Http\Controllers\User\DashboardController@searchItems')->name('user.search.items');
        });
    });
});
