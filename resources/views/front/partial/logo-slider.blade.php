<div class="container-fluid">
    <div class="brand-slider">
        <div class="brand-item"><img src="{{asset('front-assets/img/brand-1.png')}}" alt=""></div>
        <div class="brand-item"><img src="{{asset('front-assets/img/brand-2.png')}}" alt=""></div>
        <div class="brand-item"><img src="{{asset('front-assets/img/brand-3.png')}}" alt=""></div>
        <div class="brand-item"><img src="{{asset('front-assets/img/brand-4.png')}}" alt=""></div>
        <div class="brand-item"><img src="{{asset('front-assets/img/brand-5.png')}}" alt=""></div>
        <div class="brand-item"><img src="{{asset('front-assets/img/brand-6.png')}}" alt=""></div>
    </div>
</div>
