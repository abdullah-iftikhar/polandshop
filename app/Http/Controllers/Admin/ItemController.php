<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ItemController extends Controller
{
    /**
     * Class User
     * @package App\Models\User
     *
     * @property-read int $id
     */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.items.index');
    }

    /**
     * Server side pagination for get the users result.
     *
     */
    public function getPageResult(Request $request)
    {
        $totalData = Item::count();
        $totalFiltered = $totalData;
        $columns = array(
            0 => 'id',
            1 => 'name',
            2 => 'category',
            3 => 'slug',
            4 => 'original_price',
            5 => 'discount_price',
            6 => 'display_price',
            7 => 'status',
            8 => 'created_at',
            9 => 'action',
        );
        $limit = $request->input('length');
        $start = $request->input('start');
        $start = $start ? $start / $limit : 0;
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if (empty($request->input('search.value'))) {
            $items = Item::orderBy('created_at', 'desc')
                ->paginate($limit, ['*'], 'page', $start + 1);
            $totalFiltered = $totalData;
        } else {
            $search = $request->input('search.value');
            $items = Item::where('name', 'LIKE', "%{$search}%")
                ->orWhere('original_price', 'LIKE', "%{$search}%")
                ->orWhere('discount', 'LIKE', "%{$search}%")
                ->orWhere('display_price', 'LIKE', "%{$search}%")
                ->orWhere('slug', 'LIKE', "%{$search}%")
                ->with(['getCategory' => function($query) use ($search){
                    $query->where('name', 'like', '%'.$search.'%');
                }])
                ->orderBy('created_at', 'desc')
                ->paginate($limit, ['*'], 'page', $start + 1);
            $totalFiltered = $items->count();
        }
        $data = array();
        if (!empty($items)) {
            foreach ($items as $key => $item) {
                $nestedData['id'] = ($start * $limit) + $key + 1;
                $nestedData['name'] = $item->name;
                $nestedData['category'] = isset($item->getCategory)?$item->getCategory->name:"not find";
                $nestedData['original_price'] = $item->original_price;
                $nestedData['discount_price'] = $item->discount."%";
                $nestedData['display_price'] = $item->display_price;
                $nestedData['slug'] = view('admin.items.item_detail', compact('item'))->render();
                if ($item->status == 1) {
                    $nestedData['status'] = "<span class='text-success'>Active</span>";
                } else {
                    $nestedData['status'] = "<span class='text-danger'>Inactive</span>";
                }
                $nestedData['created_at'] = $item->created_at->diffForHumans();
                $status = route('item.status', encrypt($item->id));
                $edit = route('item.edit', encrypt($item->id));
                $delete = route('item.drop', encrypt($item->id));
                $exist = $item;
                $nestedData['action'] = view('admin.partial.action', compact('exist', 'status', 'edit', 'delete'))->render();
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        return json_encode($json_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('status',1)->get();
        return view('admin.items.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'category' => 'required',
            'product_name' => 'required',
            'product_thumbnail' => 'required|mimes:jpeg,jpg,gif,png',
            'product_images.*' => 'required|mimes:jpeg,jpg,gif,png',
            'product_price' => 'required | numeric',
            'discount_percentage' => 'required| integer',
            'description' => 'required',
            'specification' => 'required',
        ]);

        $item = new Item();
        $item->category_id = $request['category'];
        $item->name = $request['product_name'];
        $item->slug = time()."-".Str::slug($request['product_name']);

        if ($request->file('product_thumbnail')) {
            $productThumbnail = $request->file('product_thumbnail');
            $mainName = "thumbnail" . time() . '.' . $productThumbnail->getClientOriginalExtension();
            $productThumbnail->storeAs('public/product/thumbnail', $mainName);
            $item->thumbnail = $mainName;
            $item->thumbnail_url = asset('storage/app/public/product/thumbnail/' . $mainName);
        }
        $images = array();
        $img_url = array();

        foreach ($request['product_images'] as $key=>$img) {
            $productImg = $img;
            $mainName = "img".$key.time().'.'.$productImg->getClientOriginalExtension();
            $productImg->storeAs('public/product/images', $mainName);
            array_push($images, $mainName);
            array_push($img_url, asset('storage/app/public/product/images/' . $mainName));
        }
        $item->images = json_encode($images);
        $item->img_urls = json_encode($img_url);

        $item->original_price = $request['product_price'];
        $displayPrice = $request['product_price'] - ($request['discount_percentage']/100 * $request['product_price']);
        $item->discount = $request['discount_percentage'];
        $item->display_price = $displayPrice;

        $item->desc = $request['description'];
        $item->spec = $request['specification'];
        $item->save();
        return redirect()->route('item.index')->with('success', 'Item successfully registered.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::find(decrypt($id));
        if ($item) {
            $categories = Category::where('status',1)->get();
            return view('admin.items.edit', compact('item', 'categories'));
        } else {
            return redirect()->back()->with('error', 'Invalid record!');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Item::find(decrypt($id));
        if ($item) {
            $request->validate([
                'category' => 'required',
                'product_name' => 'required',
                'product_price' => 'required | numeric',
                'discount_percentage' => 'required| integer',
                'description' => 'required',
                'specification' => 'required',
            ]);
            $item->category_id = $request['category'];
            $item->name = $request['product_name'];
            $item->slug = time()."-".Str::slug($request['product_name']);

            if ($request->file('product_thumbnail')) {
                $request->validate([
                    'product_thumbnail' => 'required|mimes:jpeg,jpg,gif,png',
                ]);
                $productThumbnail = $request->file('product_thumbnail');
                $mainName = "thumbnail" . time() . '.' . $productThumbnail->getClientOriginalExtension();
                $productThumbnail->storeAs('public/product/thumbnail', $mainName);
                $item->thumbnail = $mainName;
                $item->thumbnail_url = asset('storage/app/public/product/thumbnail/' . $mainName);
            }
            if($request['product_images']) {
                $images = array();
                $img_url = array();
                $request->validate([
                    'product_images.*' => 'required|mimes:jpeg,jpg,gif,png',
                ]);
                foreach ($request['product_images'] as $key=>$img) {
                    $productImg = $img;
                    $mainName = "img".$key.time().'.'.$productImg->getClientOriginalExtension();
                    $productImg->storeAs('public/product/images', $mainName);
                    array_push($images, $mainName);
                    array_push($img_url, asset('storage/app/public/product/images/' . $mainName));
                }
                $item->images = json_encode($images);
                $item->img_urls = json_encode($img_url);
            }


            $item->original_price = $request['product_price'];
            $displayPrice = $request['product_price'] - ($request['discount_percentage']/100 * $request['product_price']);
            $item->discount = $request['discount_percentage'];
            $item->display_price = $displayPrice;
            $item->desc = $request['description'];
            $item->spec = $request['specification'];
            $item->save();
            return redirect()->route('item.index')->with('success', 'Item successfully updated.');
        } else {
            return redirect()->route('item.index')->with('error', 'Invalid Record!.');
        }
    }

    /**
     * Update the status of users.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function itemStatus(Request $request, $id)
    {
        try {
            $record = Item::find(decrypt($id));
            if ($record) {
                $record->status = $request['status'];
                $record->save();
                if ($request['status'] == 1) {
                    return redirect()->back()->with('success', 'Item active successfully.');
                } else {
                    return redirect()->back()->with('success', 'Item inactive successfully.');
                }
            } else {
                return redirect()->back()->with('error', 'wrong access.');
            }
        } catch (Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $record = Item::find(decrypt($id));
        if ($record) {
            $record->delete();
            return redirect()->back()->with('success', 'Item successfully deleted.');
        } else {
            return redirect()->back()->with('error', 'Invalid record!');
        }
    }
}
