<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Item;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function productList()
    {
        $items = Item::where('status', 1)->orderBy('created_at', 'desc')->paginate(9);
        return view('front.product-list', compact('items'));
    }

    public function productDetail($slug)
    {
        $item = Item::where('slug', $slug)->first();
        if ($item) {
            return view('front.product-detail', compact('item'));
        } else {
            return redirect()->back()->with('error', 'Product not find.');
        }
    }

    public function getCategoryPro($slug) {
        $category = Category::where('slug', $slug)->first();
        if ($category) {
            $items = Item::where('category_id', $category->id)->where('status',1)->paginate(9);
            return view('front.product-list', compact('items'));
        } else {
            return redirect()->back()->with('error', 'Category not find.');
        }
    }
}
