<a data-toggle="modal"
   data-target="#accDetail{{$item->id}}" class="text-info">{{$item->slug}}</a>
<div class="modal fade text-left"
     id="accDetail{{$item->id}}"
     tabindex="-1" role="dialog"
     aria-labelledby="basicModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row mb-1">
                    <div class="col-md-3">
                       <strong>Product Name:</strong>
                    </div>
                    <div class="col-md-9">
                        <p>{{isset($item->name)?$item->name:""}}</p>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-md-3">
                        <strong>Category:</strong>
                    </div>
                    <div class="col-md-9">
                        <p>{{isset($item->getCategory)?$item->getCategory->name:"not find"}}</p>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-md-3">
                        <strong>Slug:</strong>
                    </div>
                    <div class="col-md-9">
                        <p>{{isset($item->slug)?$item->slug:""}}</p>
                    </div>
                </div>

                <div class="row mb-1">
                    <div class="col-md-3">
                        <strong>Original:</strong>
                    </div>
                    <div class="col-md-9">
                        <p>{{isset($item->original_price)?$item->original_price:""}}</p>
                    </div>
                </div>

                <div class="row mb-1">
                    <div class="col-md-3">
                        <strong>Discount:</strong>
                    </div>
                    <div class="col-md-9">
                        <p>{{isset($item->discount)?$item->discount:""}}%</p>
                    </div>
                </div>

                <div class="row mb-1">
                    <div class="col-md-3">
                        <strong>Display Price:</strong>
                    </div>
                    <div class="col-md-9">
                        <p>{{isset($item->display_price)?$item->display_price:""}}</p>
                    </div>
                </div>

                <div class="row mb-1">
                    <div class="col-md-12">
                        <strong>Thumbnail:</strong>
                    </div>
                    <div class="col-md-12">
                        <img src="{{$item->thumbnail_url}}" alt="" width="100%" >
                    </div>
                </div>

                <div class="row mb-1">
                    <div class="col-md-12">
                        <strong>Product Images:</strong>
                    </div>
                    <div class="col-md-12">
                        @php
                            $images = json_decode($item->img_urls);
                        @endphp
                        @foreach($images as $image)
                            <img src="{{$image}}" alt="" width="150" height="150" >
                        @endforeach
                    </div>
                </div>

                <div class="row mb-1 mt-1">
                    <div class="col-md-12">
                        <strong>Description:</strong>
                    </div>
                    <div class="col-md-12">
                        <p>{!!isset($item->desc)?$item->desc:""!!}</p>
                    </div>
                </div>

                <div class="row mb-1 mt-1">
                    <div class="col-md-12">
                        <strong>Specifications:</strong>
                    </div>
                    <div class="col-md-12">
                        <p>{!!isset($item->spec)?$item->spec:""!!}</p>
                    </div>
                </div>


            </div>

            <div class="modal-footer">
                <button type="button"
                        class="btn grey btn-secondary"
                        data-dismiss="modal">Close
                </button>
            </div>
        </div>
    </div>
</div>
