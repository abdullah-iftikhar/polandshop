@extends('admin.layouts.layout')
@push('dashboard.scripts-head')
@endpush
@section('dashboard.content-view')
    <!-- BEGIN: Content-->
    <div class="content-header row">
        <div class="content-header-left col-md-8 col-12 mb-2 breadcrumb-new">
            <h3 class="content-header-title mb-0 d-inline-block">Admin</h3>
            <div class="breadcrumbs-top d-inline-block">
                <div class="breadcrumb-wrapper mr-1">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Item</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Create</a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    {{--    main content--}}
    <div class="content-body">
        <!-- Alert animation start -->
        <section id="configuration">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">
                                <div class="row justify-content-center">
                                    <div class="col-xl-8 col-md-8 col-sm-12">
                                        <div class="card-block">
                                            <div class="card-body">
                                                <form action="{{route('item.store')}}" method="post"
                                                      enctype="multipart/form-data">
                                                    @csrf

                                                    <fieldset class="form-group">
                                                        <label for="name">Select Category</label>
                                                        <select name="category" id="" required class="form-control">
                                                            <option selected disabled>Select Option</option>
                                                            @foreach($categories as $category)
                                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        @if($errors->has('category'))
                                                            <div class="error"
                                                                 style="color:red">{{$errors->first('category')}}</div>
                                                        @endif
                                                    </fieldset>

                                                    <fieldset class="form-group">
                                                        <label for="name"> Product Name</label>
                                                        <input type="text" name="product_name" value="{{old('product_name')}}"
                                                               placeholder="" class="form-control"
                                                               id="basicInput" required>
                                                        @if($errors->has('product_name'))
                                                            <div class="error"
                                                                 style="color:red">{{$errors->first('product_name')}}</div>
                                                        @endif
                                                    </fieldset>

                                                    <fieldset class="form-group">
                                                        <label for="description">Select Product Thumbnail</label>
                                                        <input type="file" name="product_thumbnail" required class="form-control">
                                                        @if($errors->has('product_thumbnail'))
                                                            <div class="error"
                                                                 style="color:red">{{$errors->first('product_thumbnail')}}</div>
                                                        @endif
                                                    </fieldset>

                                                    <fieldset class="form-group">
                                                        <label for="description">Select Multiple Product Images</label>
                                                        <input type="file" name="product_images[]" required class="form-control" multiple>
                                                        @if($errors->has('product_images'))
                                                            <div class="error"
                                                                 style="color:red">{{$errors->first('product_images')}}</div>
                                                        @endif
                                                    </fieldset>

                                                    <fieldset class="form-group">
                                                        <label for="name"> Product Price</label>
                                                        <input type="number" step="any" name="product_price" value="{{old('product_price')}}"
                                                               placeholder="" class="form-control"
                                                               id="basicInput" required>
                                                        @if($errors->has('product_price'))
                                                            <div class="error"
                                                                 style="color:red">{{$errors->first('product_price')}}</div>
                                                        @endif
                                                    </fieldset>

                                                    <fieldset class="form-group">
                                                        <label for="name"> Discount Percentage</label>
                                                        <input type="number" name="discount_percentage" value="{{old('discount_percentage')}}"
                                                               placeholder="" class="form-control"
                                                               id="basicInput" required>
                                                        @if($errors->has('discount_percentage'))
                                                            <div class="error"
                                                                 style="color:red">{{$errors->first('discount_percentage')}}</div>
                                                        @endif
                                                    </fieldset>

                                                    <fieldset class="form-group">
                                                        <label for="description"> Product Description</label>
                                                        <textarea name="description" class="tinymce"></textarea>
                                                        @if($errors->has('description'))
                                                            <div class="error"
                                                                 style="color:red">{{$errors->first('description')}}</div>
                                                        @endif
                                                    </fieldset>

                                                    <fieldset class="form-group">
                                                        <label for="specification"> Product Specification</label>
                                                        <textarea name="specification" class="tinymce"></textarea>
                                                        @if($errors->has('specification'))
                                                            <div class="error"
                                                                 style="color:red">{{$errors->first('specification')}}</div>
                                                        @endif
                                                    </fieldset>

                                                    <div class="row justify-content-center m-2"
                                                         style="border-top: 1px solid black">
                                                        <fieldset class="form-group center m-2">
                                                            <a href="{{route('item.index')}}"
                                                               class="btn btn-primary">View All</a>
                                                            <button type="submit" class="btn btn-success">submit
                                                            </button>
                                                        </fieldset>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Alert animation end -->
    </div>
    <!-- END: Content-->
@endsection

@push('dashboard.scripts-footer')
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ url('assets/dashboard/app-assets/vendors/js/editors/tinymce/tinymce.js')}}" type="text/javascript"></script>
    <!-- END: Page Vendor JS-->
    <!-- BEGIN: Page JS-->
    <script src="{{ url('assets/dashboard/app-assets/js/scripts/editors/editor-tinymce.js')}}" type="text/javascript"></script>
    <!-- END: Page JS-->
@endpush

