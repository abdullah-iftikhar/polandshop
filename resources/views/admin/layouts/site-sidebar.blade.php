<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true" data-img="{{ url('assets/dashboard/app-assets/images/backgrounds/02.')}}">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item m-auto">
                <a class="navbar-brand" href="#">
                    <img class="brand-logo" alt="Novel Nagri" src="{{ asset('assets/logo.png') }}" />
                </a>
            </li>

            <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>

            <li class="nav-item">
                <a href="#" class="nav-link mainmenu-icon">
                    <img class="brand-logo" alt="" src="{{asset('app-assets/images/icons/Grid_Icon1.svg')}}" />
                </a>
            </li>
        </ul>
    </div>

    <div class="navigation-background"></div>

    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

            <li class=" nav-item">
                <a href="{{route('admin.dashboard')}}">
                    <i class="ft-home"></i>
                    <span class="menu-title" data-i18n="">Dashboard</span>
                </a>
            </li>

            <li class=" nav-item">
                <a href="#">
                    <i class="ft-cast"></i>
                    <span class="menu-title" data-i18n="">Category</span>
                </a>
                <ul class="menu-content">
                    <li>
                        <a href="{{route("category.index")}}">
                            <span data-i18n="">List</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route("category.create")}}">
                            <span data-i18n="">Add</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class=" nav-item">
                <a href="#">
                    <i class="ft-disc"></i>
                    <span class="menu-title" data-i18n="">Items</span>
                </a>
                <ul class="menu-content">
                    <li>
                        <a href="{{route("item.index")}}">
                            <span data-i18n="">List</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route("item.create")}}">
                            <span data-i18n="">Add</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class=" nav-item clients">
                <a href="#">
                    <i class="ft-settings"></i>
                    <span class="menu-title" data-i18n="">Settings</span>
                </a>
                <ul class="menu-content">
                    <li>
                        <a href="{{route("admin.settings.profile")}}"><span data-i18n="">Profile</span></a>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
</div>
