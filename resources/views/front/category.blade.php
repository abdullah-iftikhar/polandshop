@extends('front.layouts.main')
@section('title', 'Categories')
@section('categories', 'active')

@section('front-content')

    <!-- Breadcrumb Start -->
    <div class="breadcrumb-wrap">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">categories</a></li>
                <li class="breadcrumb-item active">List</li>
            </ul>
        </div>
    </div>
    <!-- Breadcrumb End -->


    <!-- Category Start-->
    <div class="category">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 col-sm-12">
                    <div class="category-item ch-400">
                        <img src="{{asset('front-assets/img/category-3.jpg')}}"/>
                        <a class="category-name" href="{{route('front.product.list')}}">
                            <p>Category Name</p>
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="category-item ch-400">
                        <img src="{{asset('front-assets/img/category-3.jpg')}}"/>
                        <a class="category-name" href="{{route('front.product.list')}}">
                            <p>Category Name</p>
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="category-item ch-400">
                        <img src="{{asset('front-assets/img/category-3.jpg')}}"/>
                        <a class="category-name" href="{{route('front.product.list')}}">
                            <p>Category Name</p>
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="category-item ch-400">
                        <img src="{{asset('front-assets/img/category-3.jpg')}}"/>
                        <a class="category-name" href="{{route('front.product.list')}}">
                            <p>Category Name</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Category End-->


    <!-- Brand Start -->
    <div class="brand">
        <div class="container-fluid">
            <div class="brand-slider">
                <div class="brand-item"><img src="{{asset('front-assets/img/brand-1.png')}}" alt=""></div>
                <div class="brand-item"><img src="{{asset('front-assets/img/brand-2.png')}}" alt=""></div>
                <div class="brand-item"><img src="{{asset('front-assets/img/brand-3.png')}}" alt=""></div>
                <div class="brand-item"><img src="{{asset('front-assets/img/brand-4.png')}}" alt=""></div>
                <div class="brand-item"><img src="{{asset('front-assets/img/brand-5.png')}}" alt=""></div>
                <div class="brand-item"><img src="{{asset('front-assets/img/brand-6.png')}}" alt=""></div>
            </div>
        </div>
    </div>
    <!-- Brand End -->

@endsection
