<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    /**
     * Class User
     * @package App\Models\User
     *
     * @property-read int $id
     */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.category.index');
    }

    /**
     * Server side pagination for get the users result.
     *
     */
    public function getPageResult(Request $request)
    {
        $totalData = Category::count();
        $totalFiltered = $totalData;
        $columns = array(
            0 => 'id',
            1 => 'name',
            2 => 'slug',
            3 => 'img',
            4 => 'status',
            5 => 'created_at',
            6 => 'action',
        );
        $limit = $request->input('length');
        $start = $request->input('start');
        $start = $start ? $start / $limit : 0;
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if (empty($request->input('search.value'))) {
            $categories = Category::orderBy('created_at', 'desc')
                ->paginate($limit, ['*'], 'page', $start + 1);
            $totalFiltered = $totalData;
        } else {
            $search = $request->input('search.value');
            $categories = Category::where('name', 'LIKE', "%{$search}%")
                ->orWhere('slug', 'LIKE', "%{$search}%")
                ->orderBy('created_at', 'desc')
                ->paginate($limit, ['*'], 'page', $start + 1);
            $totalFiltered = $categories->count();
        }
        $data = array();
        if (!empty($categories)) {
            foreach ($categories as $key => $category) {
                $nestedData['id'] = ($start * $limit) + $key + 1;
                $nestedData['name'] = $category->name;
                $nestedData['slug'] = $category->slug;
                $nestedData['img'] = "<a href='$category->img_url' target='_blank'><img src='$category->img_url' alt='' width='50' height='50'></a>";
                if ($category->status == 1) {
                    $nestedData['status'] = "<span class='text-success'>Active</span>";
                } else {
                    $nestedData['status'] = "<span class='text-danger'>Inactive</span>";
                }
                $nestedData['created_at'] = $category->created_at->diffForHumans();
                $status = route('category.status', encrypt($category->id));
                $edit = route('category.edit', encrypt($category->id));
                $delete = route('category.drop', encrypt($category->id));
                $exist = $category;
                $nestedData['action'] = view('admin.partial.action', compact('exist', 'status', 'edit', 'delete'))->render();
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        return json_encode($json_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required |unique:categories| max:500',
            'image' => 'required|mimes:jpeg,jpg,gif,png',
        ]);
        $category = new Category();
        $category->name = $request['name'];
        if ($request->file('image')) {
            $profile = $request->file('image');
            $mainName = "item" . time() . '.' . $profile->getClientOriginalExtension();
            $profile->storeAs('public/category', $mainName);
            $category->img_name = $mainName;
            $category->img_url = asset('storage/app/public/category/' . $mainName);
        }
        $category->slug = Str::slug($request['name']);
        $category->save();
        return redirect()->route('category.index')->with('success', 'Category successfully registered.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find(decrypt($id));
        if ($category) {
            return view('admin.category.edit', compact('category'));
        } else {
            return redirect()->back()->with('error', 'Invalid record!');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find(decrypt($id));
        if ($category) {

            if ($request['name'] != $category->name) {
                $request->validate([
                    'name' => 'required |unique:categories| max:150',
                ]);

                $category->name = $request['name'];
                $category->slug = Str::slug($request['name']);
            }
            if ($request->file('image')) {
                $request->validate([
                    'image' => 'required|mimes:jpeg,jpg,gif,png',
                ]);
                Storage::delete('public/category/' . $category->img_name);
                $profile = $request->file('image');
                $mainName = "item" . time() . '.' . $profile->getClientOriginalExtension();
                $profile->storeAs('public/category', $mainName);
                $category->img_name = $mainName;
                $category->img_url = asset('storage/app/public/category/' . $mainName);
            }
            $category->save();
            return redirect()->route('category.index')->with('success', 'Category successfully updated.');
        } else {
            return redirect()->route('category.index')->with('error', 'Invalid record.');
        }
    }

    /**
     * Update the status of users.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function categoryStatus(Request $request, $id)
    {
        try {
            $record = Category::find(decrypt($id));
            if ($record) {
                $record->status = $request['status'];
                $record->save();
                if ($request['status'] == 1) {
                    return redirect()->back()->with('success', 'Category active successfully.');
                } else {
                    return redirect()->back()->with('success', 'Category inactive successfully.');
                }
            } else {
                return redirect()->back()->with('error', 'wrong access.');
            }
        } catch (Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $record = Category::find(decrypt($id));
        if ($record) {
            Storage::delete('public/category/' . $record->img_name);
            $record->delete();
            return redirect()->back()->with('success', 'Category successfully deleted.');
        } else {
            return redirect()->back()->with('error', 'Invalid record!');
        }
    }
}
