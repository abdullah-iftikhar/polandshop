<!-- Top bar Start -->
<div class="top-bar">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <i class="fa fa-envelope"></i>
                support@email.com
            </div>
            <div class="col-sm-6">
                <i class="fa fa-phone-alt"></i>
                +012-345-6789
            </div>
        </div>
    </div>
</div>
<!-- Top bar End -->

<!-- Nav Bar Start -->
<div class="nav">
    <div class="container-fluid">
        <nav class="navbar navbar-expand-md bg-dark navbar-dark">
            <a href="#" class="navbar-brand">MENU</a>
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                <div class="navbar-nav mr-auto">
                    <a href="{{route('front.home')}}" class="nav-item nav-link @yield('home', '')">Home</a>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Categories</a>
                        <div class="dropdown-menu">
                            @php
                                $categories = App\Models\Category::where('status',1)->get();
                            @endphp
                            @if($categories->count() > 0)
                            @foreach($categories as $category)
                            <a href="{{route('front.category.products', $category->slug)}}" class="dropdown-item">{{$category->name}}</a>
                            @endforeach
                            @else
                                <span style="color: white">Category not find.</span>
                            @endif
                        </div>
                    </div>
{{--                    <a href="{{route('front.categories')}}" class="nav-item nav-link @yield('categories', '')">Categories</a>--}}
                    <a href="{{route('front.product.list')}}" class="nav-item nav-link @yield('products', '')">Products</a>
                    <a href="{{route('front.best.seller')}}" class="nav-item nav-link @yield('best_seller', '')">Best Seller</a>
                    <a href="{{route('front.cart')}}" class="nav-item nav-link @yield('cart', '')">Cart</a>
                    <a href="{{route('front.checkout')}}" class="nav-item nav-link @yield('checkout', '')">Checkout</a>

                </div>
                <div class="navbar-nav ml-auto">
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">User Account</a>
                        <div class="dropdown-menu">
                            <a href="{{route('front.account')}}" class="dropdown-item">My Account</a>
                            <a href="{{route('front.login')}}" class="dropdown-item">Login</a>
                            <a href="{{route('front.login')}}" class="dropdown-item">Register</a>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</div>
<!-- Nav Bar End -->

<!-- Bottom Bar Start -->
<div class="bottom-bar">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-3">
                <div class="logo">
                    <a href="{{route('front.home')}}">
                        <img src="{{asset('front-assets/img/logo.png')}}" alt="Logo">
                    </a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="search">
                    <input type="text" placeholder="Search">
                    <button><i class="fa fa-search"></i></button>
                </div>
            </div>
            <div class="col-md-3">
                <div class="user">
                    <a href="{{route('front.wishlist')}}" class="btn wishlist">
                        <i class="fa fa-heart"></i>
                        <span>(0)</span>
                    </a>
                    <a href="{{route('front.cart')}}" class="btn cart">
                        <i class="fa fa-shopping-cart"></i>
                        <span>(0)</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Bottom Bar End -->
